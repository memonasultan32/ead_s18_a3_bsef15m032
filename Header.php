    <html>
	<head>
		<meta charset="utf-8">
    <link rel="stylesheet" href="mystyle.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap.min.js"></script>
	<script src="jquery-3.2.1.min.js" type="text/javascript"></script>
   
    <style>
        body {
            background: /* On "top" */
            repeating-linear-gradient( 135deg, transparent, transparent 1px, #ccc 1px, #ccc 2px ),
            /* on "bottom" */
            linear-gradient( to bottom, #eee, #999 );
            font-family: Arial;
        }
    </style>
	</head>
<body>
	<nav style=" " class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul style="font-size:17px;font-family:Consolas" class="nav navbar-nav">
              
				<li>
                    <a href="Home.php">Home</a>
                </li>
                <li  >
                    <a href="user.php"><b>User Management</b></a>
                </li>
                <li>
                    <a href="Role.php"><b>Role Management</b></a>
                </li>
                <li>
                    <a href="Permission.php"><b>Permission Management</b></a>
                </li>
                <li>
                    <a href="RolePermission.php"><b>Role-Permission Management</b></a>
                </li>
                <li>
                    <a href="UserRole.php"><b>User-Role Management</b></a>
                </li>
                <li>
                    <a href="Logout.php"><b>Log Out</b></a>
                </li>
				 <li>
                    <a href="LoginHistory.php"><b>History</b></a>
                </li>
            </ul>
        </div>
    </nav>
</body>
</html>