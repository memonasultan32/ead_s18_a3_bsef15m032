<?php
	require('conn.php');
session_start();
	$action = $_REQUEST['act'];
	
	
	if($action=="loadCities"){
		$country_id = $_REQUEST["CID"];		
		$cities = array();
		
		$sql = "select * from city where countryid=$country_id";
						$res = mysqli_query($conn, $sql);
						$records= mysqli_num_rows($res);
						if($records>0)
						{	
							$i=0;
							while($row = mysqli_fetch_assoc($res))
							{
								$cid = $row['id'];
								$cname = $row['name'];
								$cities[$i] = array("CityID"=>$cid , "Name"=> $cname);
								$i= $i+1;
							}
						}
		
		$output["Cities"] = $cities;
		echo json_encode($output);
	}
	else if($action=="saveUser"){
		
		$output["Edit"]=array();
		$output["New"]=array();
		$error="";
		$uid = $_REQUEST['uid'];
		$ulogin = $_REQUEST['login'];
		$uname = $_REQUEST['name'];
		$uemail = $_REQUEST['email'];
		$upass = $_REQUEST['pass'];
		$ucon = $_REQUEST['country'];
		$ucity = $_REQUEST['city'];
		$creator = $_REQUEST['creator'];
		$time = $_REQUEST['time'];	
		$utype = $_REQUEST['is_admin'];
			
		
		$sql = "select * from users where userid='$uid'";
		$res = mysqli_query($conn, $sql);	
		$result = mysqli_num_rows($res);
		
		// It means its already have user with this id 
		// so we'll edit user 
		if($result>0){
			
			$sql = "UPDATE users SET name='$uname', email='$uemail', password='$upass' , countryid=$ucon , cityid=$ucity, isadmin=$utype
			where userid=$uid";
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result update row
				$output["Edit"]= array("uid"=>$uid,
				"login"=>$ulogin,
			"name"=>$uname,
			"email"=>$uemail,
			"pass"=>$upass,
			"country"=>$ucon,
			"city"=>$ucity,
			"creator"=>$creator,
			"time"=>$time,
			"is_admin"=>$utype,
				);
			}
		}
		else{
			
		$sql = "select * from users where login='$ulogin'";
		$res = mysqli_query($conn, $sql);	
		$result = mysqli_num_rows($res);
		if($result==0)
		{
			$creator = $_SESSION['id'];
			$time = date("Y-m-d H:i:s");
			// otherwise new user saved with createdBy & createdOn
			$sql = "Insert into users(userid,login,password,name,email,countryid,cityid,createdon,createdby,isadmin)
					values(DEFAULT,'$ulogin','$upass','$uname','$uemail',$ucon,$ucity,'$time',$creator,$utype)";	
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result and add a new row
				$sql = "select userid from users where login='$ulogin'";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$uid = $row['userid'];
				}
				$output["New"]= array("uid"=>$uid,
				"login"=>$ulogin,
			"name"=>$uname,
			"email"=>$uemail,
			"pass"=>$upass,
			"country"=>$ucon,
			"city"=>$ucity,
			"creator"=>$creator,
			"time"=>$time,
			"is_admin"=>$utype,
				);
			}
		}
	}
	echo json_encode($output);
	}
	else if($action=="delUser"){
	
			$id = $_REQUEST["id"];		
		
		$sql = "delete from users where userid=$id";
						$res = mysqli_query($conn, $sql);
						if($res==true)
							echo $id;
		
	
	}
	else if($action=="getUserbyID"){
		
		$user = array();
		$error="";
		$x = "readonly='readonly'";
		$uid = $_REQUEST['id'];
		
		$sql = "select * from users where userid='".$uid."'";
		$result = mysqli_query($conn, $sql);
		$records = mysqli_num_rows($result);
		if($records>0)
		{
			$i=0;
			while($row= mysqli_fetch_assoc($result))
			{
				$id = $row['userid'];
				$login = $row['login'];
				$email = $row['email'];
				$name = $row['name'];
				$password=$row['password'];
				$country = $row['countryid'];
				$city = $row['cityid'];
				$t = $row['isadmin'];
				$creator = $row['createdby'];
				$time = $row['createdon'];
				
				$user[$i] = array("uid"=>$uid,
				"login"=>$login,
			"name"=>$name,
			"email"=>$email,
			"pass"=>$password,
			"country"=>$country,
			"city"=>$city,
			"creator"=>$creator,
			"time"=>$time,
			"is_admin"=>$t,
				);
				$i=$i+1;
			}
		}
		
		$output["user"]=$user;
		echo json_encode($output);
	}
	else if($action=="saveRole"){
		
		$output["Edit"]=array();
		$output["New"]=array();
		$error="";
		$role = $_REQUEST['role'];
		$des = $_REQUEST['des'];
		$c= $_REQUEST['creator'];
		$t = $_REQUEST['time'];
			
		
		$sql = "select * from roles where name='$role'";
		$res = mysqli_query($conn, $sql);	
		$result = mysqli_num_rows($res);
		
		// It means its already have user with this id 
		// so we'll edit user 
		if($result>0){
			
			$sql = "UPDATE roles SET description='$des' where name='$role'";
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result update row
				$output["Edit"]= array(
				"des"=>$des,
			"role"=>$role,
			"creator"=>$c,
			"time"=>$t
				);
			}
		}
		else{
			
	
			$creator = $_SESSION['id'];
			$time = date("Y-m-d H:i:s");
			// otherwise new user saved with createdBy & createdOn
			$sql = "Insert into roles(roleid,name,description,createdon,createdby)
					values(DEFAULT,'$role','$des','$time',$creator)";	
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result and add a new row
				$sql = "select roleid from roles where name='$role'";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$uid = $row['roleid'];
				}
				$output["New"]= array("rid"=>$uid,
				"des"=>$des,
			"role"=>$role,
			"creator"=>$c,
			"time"=>$t
				);
			}
		
	}
	echo json_encode($output);
	}
	else if($action=="delRole"){
		
			$id = $_REQUEST["id"];		
		
		$sql = "delete from roles where roleid=$id";
						$res = mysqli_query($conn, $sql);
						if($res==true)
							echo $id;
		
	}
	else if($action=="getRolebyID"){
		
				$user = array();
		$error="";
		$x = "readonly='readonly'";
		$uid = $_REQUEST['id'];
		
		$sql = "select * from roles where roleid='".$uid."'";
		$result = mysqli_query($conn, $sql);
		$records = mysqli_num_rows($result);
		if($records>0)
		{
			$i=0;
			while($row= mysqli_fetch_assoc($result))
			{
				$id = $row['roleid'];
				$role = $row['name'];
				$des = $row['description'];
				
				$creator = $row['createdby'];
				$time = $row['createdon'];
				
				$user[$i] = array("rid"=>$id,
				"role"=>$role,
			"des"=>$des,
			"creator"=>$creator,
			"time"=>$time,
			
				);
				$i=$i+1;
			}
		}
		
		$output["user"]=$user;
		echo json_encode($output);
		
	}
	else if($action=="savePrem"){
		
		$output["Edit"]=array();
		$output["New"]=array();
		$error="";
		$role = $_REQUEST['prem'];
		$des = $_REQUEST['des'];
		$c= $_REQUEST['creator'];
		$t = $_REQUEST['time'];
			
		
		$sql = "select * from permissions where name='$role'";
		$res = mysqli_query($conn, $sql);	
		$result = mysqli_num_rows($res);
		
		// It means its already have user with this id 
		// so we'll edit user 
		if($result>0){
			
			$sql = "UPDATE permissions SET description='$des' where name='$role'";
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result update row
				$output["Edit"]= array(
				"des"=>$des,
			"prem"=>$role,
			"creator"=>$c,
			"time"=>$t
				);
			}
		}
		else{
			
	
			$creator = $_SESSION['id'];
			$time = date("Y-m-d H:i:s");
			// otherwise new user saved with createdBy & createdOn
			$sql = "Insert into permissions(permissionid,name,description,createdon,createdby)
					values(DEFAULT,'$role','$des','$time',$creator)";	
			$res = mysqli_query($conn, $sql);	
			if($res==true)
			{
				// prepare result and add a new row
				$sql = "select permissionid from permissions where name='$role'";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$uid = $row['permissionid'];
				}
				$output["New"]= array("rid"=>$uid,
				"des"=>$des,
			"prem"=>$role,
			"creator"=>$c,
			"time"=>$t
				);
			}
		
	}
	echo json_encode($output);
	}
	else if($action=="delPrem"){
		
			$id = $_REQUEST["id"];		
		
		$sql = "delete from permissions where permissionid=$id";
						$res = mysqli_query($conn, $sql);
						if($res==true)
							echo $id;
		
	}
	else if($action=="getPrembyID"){
		
				$user = array();
		$error="";
		$x = "readonly='readonly'";
		$uid = $_REQUEST['id'];
		
		$sql = "select * from permissions where permissionid='".$uid."'";
		$result = mysqli_query($conn, $sql);
		$records = mysqli_num_rows($result);
		if($records>0)
		{
			$i=0;
			while($row= mysqli_fetch_assoc($result))
			{
				$id = $row['permissionid'];
				$role = $row['name'];
				$des = $row['description'];
				
				$creator = $row['createdby'];
				$time = $row['createdon'];
				
				$user[$i] = array("rid"=>$id,
				"prem"=>$role,
			"des"=>$des,
			"creator"=>$creator,
			"time"=>$time,
			
				);
				$i=$i+1;
			}
		}
		
		$output["user"]=$user;
		echo json_encode($output);
		
	}
	
	else if($action=="saveRolePrem")
	{
		$output["Edit"]=array();
		$output["New"]=array();
		$error="";
		$role = $_REQUEST['role'];
		$prem = $_REQUEST['prem'];
		$mode=$_REQUEST['mode'];
		
			
		
		if($mode=='edit'){
			$urid = $_REQUEST['rpid'];
			$sql = "UPDATE role_permission SET roleid=$role
			, permissionid=$prem where id=$urid" ;
		$res = mysqli_query($conn, $sql);
		if($res==true){
			
											$sql = "select name from roles where roleid=$role";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$role = $row['name'];
				}
				
				$sql = "select name from permissions where permissionid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$prem = $row['name'];
				}
			$output["Edit"]= array(
			"rpid"=>$urid,
				"role"=>$role,
			"prem"=>$prem,
			
				);
		}
		}
		else if($mode=='new')
		{
			$sql = "select * from role_permission where roleid=$role && permissionid=$prem";
			$res = mysqli_query($conn, $sql);	
			$result = mysqli_num_rows($res);
			if($result>0){
				
			}
			else
			{
			// otherwise new role saved with createdBy & createdOn
			$sql = "Insert into role_permission(id,permissionid,roleid)
					values(DEFAULT,$prem,$role)";	
			$res = mysqli_query($conn, $sql);
			if($res==true)
			{
				$sql = "select id from role_permission where roleid=$role && permissionid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$uid = $row['id'];
				}
				
								$sql = "select name from roles where roleid=$role";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$role = $row['name'];
				}
				
				$sql = "select name from permissions where permissionid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$prem = $row['name'];
				}
				$output["New"]= array(
				"rpid"=>$uid,
				"role"=>$role,
			"prem"=>$prem
			
				);
			}
			
			}
		}

		
	
	echo json_encode($output);
	}
	else if($action=="delRolePrem"){
		
			$id = $_REQUEST["id"];		
		
		$sql = "delete from role_permission where id=$id";
						$res = mysqli_query($conn, $sql);
						if($res==true)
							echo $id;
		
	}
	else if($action=="getRolePrembyID"){
		
				$user = array();
		$error="";
		$x = "readonly='readonly'";
		$uid = $_REQUEST['id'];
		
		$sql = "select * from role_permission where id='".$uid."'";
		$result = mysqli_query($conn, $sql);
		$records = mysqli_num_rows($result);
		if($records>0)
		{
			$i=0;
			while($row= mysqli_fetch_assoc($result))
			{
				$id = $row['id'];
				$role = $row['roleid'];
				$prem = $row['permissionid'];
				
			
				
				$user[$i] = array("rid"=>$id,
				"role"=>$role,
			"prem"=>$prem,
			
			
				);
				$i=$i+1;
			}
		}
		
		$output["user"]=$user;
		echo json_encode($output);
		
	}else if($action=="loadRoles"){
		
		$cities = array();
		
		$sql = "select * from roles";
						$res = mysqli_query($conn, $sql);
						$records= mysqli_num_rows($res);
						if($records>0)
						{	
							$i=0;
							while($row = mysqli_fetch_assoc($res))
							{
								$cid = $row['roleid'];
								$cname = $row['name'];
								$cities[$i] = array("RoleID"=>$cid , "Name"=> $cname);
								$i= $i+1;
							}
						}
		
		$output["Roles"] = $cities;
		echo json_encode($output);
	}else if($action=="loadPermissions"){
		
		$cities = array();
		
		$sql = "select * from permissions";
						$res = mysqli_query($conn, $sql);
						$records= mysqli_num_rows($res);
						if($records>0)
						{	
							$i=0;
							while($row = mysqli_fetch_assoc($res))
							{
								$cid = $row['permissionid'];
								$cname = $row['name'];
								$cities[$i] = array("PermissionID"=>$cid , "Name"=> $cname);
								$i= $i+1;
							}
						}
		
		$output["Permissions"] = $cities;
		echo json_encode($output);
	}
	else if($action=="saveRoleUser")
	{
		$output["Edit"]=array();
		$output["New"]=array();
		$error="";
		$role = $_REQUEST['role'];
		$prem = $_REQUEST['user'];
		$mode=$_REQUEST['mode'];
		
			
		
		if($mode=='edit'){
			$urid = $_REQUEST['rpid'];
			$sql = "UPDATE user_role SET roleid=$role
			, userid=$prem where id=$urid" ;
		$res = mysqli_query($conn, $sql);
		if($res==true){
			
											$sql = "select name from roles where roleid=$role";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$role = $row['name'];
				}
				
				$sql = "select name from users where userid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$prem = $row['name'];
				}
			$output["Edit"]= array(
			"rpid"=>$urid,
				"role"=>$role,
			"user"=>$prem,
			
				);
		}
		}
		else if($mode=='new')
		{
			$sql = "select * from user_role where roleid=$role && userid=$prem";
			$res = mysqli_query($conn, $sql);	
			$result = mysqli_num_rows($res);
			if($result>0){
				
			}
			else
			{
			// otherwise new role saved with createdBy & createdOn
			$sql = "Insert into user_role(id,userid,roleid)
					values(DEFAULT,$prem,$role)";	
			$res = mysqli_query($conn, $sql);
			if($res==true)
			{
				$sql = "select id from user_role where roleid=$role && userid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$uid = $row['id'];
				}
				
								$sql = "select name from roles where roleid=$role";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$role = $row['name'];
				}
				
				$sql = "select name from users where userid=$prem";
				$res = mysqli_query($conn, $sql);	
				$result = mysqli_num_rows($res);
				while($row=mysqli_fetch_assoc($res)){
					$prem = $row['name'];
				}
				$output["New"]= array(
				"rpid"=>$uid,
				"role"=>$role,
			"user"=>$prem
			
				);
			}
			
			}
		}

		
	
	echo json_encode($output);
	}
	else if($action=="delRoleUser"){
		
			$id = $_REQUEST["id"];		
		
		$sql = "delete from user_role where id=$id";
						$res = mysqli_query($conn, $sql);
						if($res==true)
							echo $id;
		
	}
	else if($action=="getRoleUserbyID"){
		
				$user = array();
		$error="";
		$x = "readonly='readonly'";
		$uid = $_REQUEST['id'];
		
		$sql = "select * from user_role where id='".$uid."'";
		$result = mysqli_query($conn, $sql);
		$records = mysqli_num_rows($result);
		if($records>0)
		{
			$i=0;
			while($row= mysqli_fetch_assoc($result))
			{
				$id = $row['id'];
				$role = $row['roleid'];
				$prem = $row['userid'];
				
			
				
				$user[$i] = array("rid"=>$id,
				"role"=>$role,
			"user"=>$prem,
			
			
				);
				$i=$i+1;
			}
		}
		
		$output["user"]=$user;
		echo json_encode($output);
		
	}else if($action=="loadUsers"){
		
		$cities = array();
		
		$sql = "select * from users";
						$res = mysqli_query($conn, $sql);
						$records= mysqli_num_rows($res);
						if($records>0)
						{	
							$i=0;
							while($row = mysqli_fetch_assoc($res))
							{
								$cid = $row['userid'];
								$cname = $row['name'];
								$cities[$i] = array("UserID"=>$cid , "Name"=> $cname);
								$i= $i+1;
							}
						}
		
		$output["Users"] = $cities;
		echo json_encode($output);
	}
	
  
?>