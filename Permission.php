<?php require('conn.php'); 
		require('Header.php');
		session_start();
		if(isset($_SESSION["id"])!= true)
{
	header('Location: login.php');
}
else if($_SESSION['type']==0)
{
	header('Location: login.php');
}

?>
<html>
<head>
<script>
function Main()
{

$(document).ready(function()
		{
			
		
		
		$("#save").click(function(){
				var error="";
		var role = $('#prem').val();
		 var des = $('#des').val();
		var creator = $('#creator').val();
		var time = $('#time').val();
		
		
			var dataToSend = {"prem":role,"des":des,"creator":creator,"time":time,"act":"savePrem"};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					if(result.Edit.length!=0){
						
						var tableRow = $("td").filter(function() {
							return $(this).text() == result.Edit.prem;
						}).closest("tr");
						var r = tableRow.find('td').first().text();
						var td = tableRow.find('td').first();
						td.text(r);
						td = td.next();
						td.text(result.Edit.prem);
						td = td.next();
						td.text(result.Edit.des);
						td = td.next();
						td.text(result.Edit.creator);
						td = td.next();
						td.text(result.Edit.time);
						
						
					}
					else if(result.New.length!=0)
					{
						
						
						var tab = $('#tbody');
						var row = $("<tr>");
						var col = $("<td>").text(result.New.rid);
						row.append(col);
						col = $("<td>").text(result.New.prem);
						row.append(col);
						col = $("<td>").text(result.New.des);
						row.append(col);
						col = $("<td>").text(result.New.creator);
						row.append(col);
						col = $("<td>").text(result.New.time);
						row.append(col);
						col = $("<td>").html("<a class='edit'> Edit </a>");
						row.append(col);
						col = $("<td>").html("<a class='del'> Delete </a>");
						row.append(col);
						tab.append(row);
					}
				
					
				}
			};
			$.ajax(settings);
		
			return false;			
		});
		

		
	$('.del').click(function ()
		{
			 col = $(this);
			  var uid = $(this).closest('tr').find('td').first().text();
			var ret = confirm("Do you want to delete the record with RoleId " + uid + "?");
			if(ret==true)
			{
				
				var dataToSend = {"act":"delPrem","id":uid};
				var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
					col.closest('tr').remove();
					
				}};
				$.ajax(settings);
				return false;
					
		
			}
			
		});
		
		
		
		$('.edit').click(function ()
		{

            var uid = $(this).closest('tr').find('td').first().text();
			var dataToSend = {"act":"getPrembyID","id":uid};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
						$('#prem').val(result.user[0].prem);
						$('#prem').attr('readonly','readonly');
						$('#des').val(result.user[0].des);
						$('#creator').val(result.user[0].creator);
						$('#time').val(result.user[0].time);
			}};
				$.ajax(settings);
				return false;
					
		});
		
	});//end of ready
	
	
		function gid(id) {
            return document.getElementById(id);
        }

	function clearAll()
	{
	
    gid('prem').value="";
    gid('des').value="";
    gid('creator').value="";
    gid('time').value="";
      
	}

		
	
		$('#clear').click(clearAll);
		
		
		
	}





</script>
</head>
<?php 
// load data if opened in edited mode
		$id ="";
		$role = "";
		$des = "";
	
		$x="";
		$creator = $_SESSION['id'];
		$time = date("Y-m-d H:i:s");  
	
		
	
?>
<body onload="Main()">
<div class="cont">
        
		<center>
		<div class="contained">
            <h1>Permission</h1>
        </div>
        <form style="padding:10px;background-color:white;width:500px;height:450px;" action="Role.php">
            <div class="form-group">
			
                <label for="usr">Premission:</label>
                <input type="text" class="form-control" id="prem" name="prem" value='<?php echo $role?>' <?php echo $x ?>>
                <br />
                <label for="usr">Description:</label>
                <input type="text" class="form-control" id="des" name="des" value='<?php echo $des ?>' required>
                <br />
				<label for="usr">Created By:</label>
                <input type="text" class="form-control" id="creator" name="creator" value='<?php echo $creator ?>' readonly='readonly'>
                <br />
				<label for="usr">Created on:</label>
                <input type="text" class="form-control" id="time" name="time" value='<?php echo $time ?>' readonly='readonly'>
                <br />
				
				<input style="float:right;" class="btn btn-default" type="submit" name="save" value="Save" id='save'/>
				
	
				<button style="float:left;" class="btn btn-default"  id='clear' onclick='clearAll()'>Clear</button>
				<br/>
				
				
				</div>
		</form>
</center>
</div>
            
			<div class="container">
  <h2>Permission List</h2>      
  <table class="table table-bordered" style="background-color:white;" id="pTable">
    <thead>
      <tr>
        <th>ID</th>
        <th>Permission</th>
        <th>Description</th>
		<th>Created by</th>
		<th>Created On</th>
		<th> Edit </th>
		<th> Delete </th>
      </tr>
    </thead>
    <tbody id='tbody'>
     <?php
	
	$sql = "select * from permissions";
	$res = mysqli_query($conn ,$sql);
	$records = mysqli_num_rows($res);
	if($records>0){
		while($row = mysqli_fetch_assoc($res)) {
			
				$id = $row['permissionid'];
				$role = $row['name'];
				$des = $row['description'];
				$creator = $row['createdby'];
				$time = $row['createdon'];
				echo "<tr> 
				<td>$id</td>
				<td>$role</td>
				<td>$des</td>
				<td>$creator</td>
				<td>$time</td>";

		echo "<td><a class='edit'> Edit </a></td>";
		echo "<td><a class='del'> Delete</a></td></tr>";
							
			}
	}
	

?>
    </tbody>
  </table>
</div>

			</body>
			</html>