<?php
require('conn.php');
require('Header.php');
session_start();
if(isset($_SESSION["id"])!= true)
{
	header('Location: login.php');
}else if($_SESSION['type']==0)
{
	header('Location: login.php');
}
?>
<html>

<head>
	<title>Login History</title>
</head>

<body>
<div class="container">
  <h2>Users List</h2>      
  <table class="table table-bordered" style="background-color:white;" id="pTable">
    <thead>
      <tr>
        <th>ID</th>
        <th>User-ID</th>
        <th>Login </th>
		<th>Login Time</th>
		<th>Machine Ip</th>
      </tr>
    </thead>
    <tbody>
<?php
	$sql = "select * from loginhistory";
	$res = mysqli_query($conn , $sql);
	$records = mysqli_num_rows($res);
	if($records>0)
	{
		while($row = mysqli_fetch_assoc($res))
		{
			$id = $row['id'];
			$login = $row['login'];
			$userid = $row['userid'];
			$time = $row['logintime'];
			$ip = $row['machineip'];
			echo "<tr>
					<td>$id</td>
					<td>$userid</td>
					<td>$login</td>
					<td>$time</td>
					<td>$ip</td></tr>";
		}
	}
?>
    </tbody>
  </table>
</div>


</body>

</html>