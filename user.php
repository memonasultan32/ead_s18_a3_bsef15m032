<html>
<?php require('conn.php'); 
		require('Header.php');
		session_start();
if(isset($_SESSION["id"])!= true)
{
	header('Location: login.php');
}
else if($_SESSION['type']==0)
{
	header('Location: login.php');
}

?>
<head>
<script>
function Main() {
		$(document).ready(function()
		{
			loadCities($("#country").val());
			
		$("#country").change(function(){
			loadCities($("#country").val());
			return false;			
		});
		
		
		// save user through ajax
		$("#save").click(function(){
				var error="";
		 var uid = $('#uid').val();
		var ulogin = $('#login').val();
		 var uname = $('#name').val();
		var uemail = $('#email').val();
		var upass = $('#pass').val();
		var ucon = $('#country').val();
		var ucity = $('#city').val();
		var creator = $('#creator').val();
		var time = $('#time').val();
		var admin = $('input:checkbox[id=is_admin]').prop("checked");
		if(admin==true)
			admin = 1;
		else
			admin =0;
		
		if(uid=="")
			uid=0;
			var dataToSend = {"uid":uid,"login":ulogin,"name":uname,"email":uemail,"pass":upass,"country":ucon,"city":ucity,
			"creator":creator,"time":time,"is_admin":admin,"act":"saveUser"};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					if(result.Edit.length!=0){
						
						var id = result.Edit.uid;
						var tableRow = $("td").filter(function() {
							return $(this).text() == id;
						}).closest("tr");
						
						var td = tableRow.find('td').first();
						td.text(id);
						td = td.next();
						td.text(result.Edit.creator);
						td = td.next();
						td.text(result.Edit.time);
						td = td.next();
						td.text(result.Edit.login);
						td = td.next();
						td.text(result.Edit.email);
						td = td.next();
						td.text(result.Edit.name);
						td = td.next();
						td.text(result.Edit.country);
						td = td.next();
						td.text(result.Edit.city);
						td = td.next();
						if(result.Edit.is_admin==1)
							td.text('Admin');
						else
							td.text('User');
						
					}
					else if(result.New.length!=0)
					{
						
						var id = result.New.uid;
						var tab = $('#tbody');
						var row = $("<tr>");
						var col = $("<td>").text(id);
						row.append(col);
						col = $("<td>").text(result.New.creator);
						row.append(col);
						col = $("<td>").text(result.New.time);
						row.append(col);
						col = $("<td>").text(result.New.login);
						row.append(col);
						col = $("<td>").text(result.New.email);
						row.append(col);
						col = $("<td>").text(result.New.name);
						row.append(col);
						col = $("<td>").text(result.New.country);
						row.append(col);
						col = $("<td>").text(result.New.city);
						row.append(col);
						col = $("<td>");
						if(result.New.is_admin==1)
							col.text('Admin');
						else
							col.text('User');
						row.append(col);
						col = $("<td>").html("<a class='edit'> Edit </a>");
						row.append(col);
						col = $("<td>").html("<a class='del'> Delete </a>");
						row.append(col);
						tab.append(row);
					}
				
					
				}
			};
			$.ajax(settings);
		
			return false;			
		});
		

		
	$('.del').click(function ()
		{
			 col = $(this);
			  var uid = $(this).closest('tr').find('td').first().text();
			var ret = confirm("Do you want to delete the record with userId " + uid + "?");
			if(ret==true)
			{
				
				var dataToSend = {"act":"delUser","id":uid};
				var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
					col.closest('tr').remove();
					
				}};
				$.ajax(settings);
				return false;
					
		
			}
			
		});
		
		
		
		$('.edit').click(function ()
		{
			
            var uid = $(this).closest('tr').find('td').first().text();
			var dataToSend = {"act":"getUserbyID","id":uid};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
						$('#uid').val(result.user[0].uid);
						$('#login').val(result.user[0].login);
						$('#email').val(result.user[0].email);
						$('#pass').val(result.user[0].pass);
						$('#creator').val(result.user[0].creator);
						$('#time').val(result.user[0].time);
						$('#name').val(result.user[0].name);
						$('#country').val(result.user[0].country).prop('selected', true);
						loadCities(result.user[0].country);
						$('#city').val(result.user[0].city).prop('selected',true);
						
						if(result.user[0].is_admin==1)
							$('input:checkbox[name=is_admin]').attr('checked',true).attr("value",1);
						else
							$('input:checkbox[name=is_admin]').removeAttr('checked');
			}};
				$.ajax(settings);
				return false;
					
		});
		
	});//end of ready
	
	
		function gid(id) {
            return document.getElementById(id);
        }

	function clearAll()
	{
	gid('uid').value="";
    gid('login').value="";
    gid('name').value="";
    gid('pass').value="";
    gid('email').value="";
    gid('country').value="";
	gid('city').value="";
	gid('is_admin').checked=false;          
	}

		function loadCities(countryId){
			var dataToSend = {"CID":countryId,"act":"loadCities"};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					
					$("#city").empty();
					for(var i=0;i<result.Cities.length;i++)
					{
						var city = result.Cities[i];
						if($('#city').val()==city.CityID)
							var opt = $("<option value="+ city.CityID +"  selected>"+city.Name+"</option>");
						else
							var opt = $("<option value="+ city.CityID +">"+city.Name+"</option>");
						$("#city").append(opt);						
					}
				}
			};
			$.ajax(settings);
						
		}
	
		$('#clear').click(clearAll);
		
		
		
	}

	</script>
<?php 

		$x="";
		$error="";
		$creator = $_SESSION['id'];
		$time = date("Y-m-d H:i:s");  
		$id = "";

				$login = "";
				$email = "";
				$name = "";
				$country = "";
				$city = "";
				
		
	
?>

</head>

<body onload="Main()">
<div class="cont">
        
	<center>	
		<div class="contained">
            <h1>Users</h1>
        </div>
        <div style="padding:10px;background-color:white;width:500px;height:auto;">
            <div class="form-group">
				<label for="usr">ID:</label>
                <input type="text" class="form-control" id="uid" name="uid" placeholder="default" value='<?php echo $id ?>' readonly='readonly'>
                <br />
				<label for="usr">Created By:</label>
                <input type="text" class="form-control" id="creator" name="creator" value='<?php echo $creator ?>' readonly='readonly'>
                <br />
                <label for="usr">Created on:</label>
                <input type="text" class="form-control" id="time" name="time" value='<?php echo $time ?>' readonly='readonly'>
                <br />
                <label for="usr">Login:</label>
                <input type="text" class="form-control" id="login" name="login" value='<?php echo $login ?>' <?php echo $x ?>>
				<span style="color:red"><?php echo $error?></span>
                <br />
                <label for="usr">Password:</label>
                <input type="text" class="form-control" id="pass" name="pass" value='<?php echo $password ?>' required>
                <br />
                <label for="usr">Name:</label>
                <input type="text" class="form-control" id="name" name="name" value='<?php echo $name ?>' required>
                <br />
                <label for="usr">Email:</label>
                <input type="email" class="form-control" id="email" name="email" value='<?php echo $email ?>' required>
                <br />
                <label for="usr">Country:</label>
                <select class="form-control" id="country" name="country" >
				
<?php  
				// load countries
						$sql = "select * from country";
						$res = mysqli_query($conn, $sql);
						$records= mysqli_num_rows($res);
						if($records>0)
						{
							while($row = mysqli_fetch_assoc($res))
							{
								$cid = $row['id'];
								$cname = $row['name'];
								if($cid==$country)
									echo "<option value=$cid selected> $cname </option>";
								else
									echo "<option value=$cid> $cname </option>";
							}
						}
?>
				</select>
				
				<label for="usr">City:</label>
                <select class="form-control" id="city" name="city" onload='loadCities(<?php echo $country ?>)' ></select>
				
				
				
                <br />
               
				
				<label for="usr">Is this user Admin? </label>
				<input type="checkbox" id="is_admin" name="is_admin" />
				<button style="float:left;" class="btn btn-default"  id='clear' >Clear</button>
				<button style="float:right;" class="btn btn-default"  name="save" value="Save" id='save'>Save</button>
			</div>
</center>
            </div>
			
			
				<div class="container">
  <h2>Users List</h2>      
  <table class="table table-bordered" style="background-color:white;" id="pTable">
    <thead>
      <tr>
        <th>ID</th>
		<th>Created by</th>
		<th>Created On</th>
        <th>Login</th>
        <th>Email</th>
		<th>Name</th>
		<th>Country ID</th>
		<th>City ID</th>
		<th> Type </th>
		<th> Edit </th>
		<th> Delete </th>
      </tr>
    </thead>
    <tbody id="tbody">
<?php
	
	$sql = "select * from users";
	$res = mysqli_query($conn ,$sql);
	$records = mysqli_num_rows($res);
	if($records>0){
		while($row = mysqli_fetch_assoc($res)) {
			
				$id = $row["userid"];
				$creator = $row['createdby'];
				$time = $row['createdon'];
				$login = $row["login"];
				$email = $row["email"];
				$name = $row["name"];
				$country = $row["countryid"];
				$city = $row["cityid"];
				$admin = $row["isadmin"];
				echo "<tr> 
				<td>$id</td>
				<td>$creator</td>
				<td>$time</td>
				<td>$login</td>
				<td>$email</td>
				<td>$name</td>
				<td>$country</td>
				<td>$city</td>";
		if($admin==1)
			echo "<td> Admin </td>";
		else
			echo "<td> User </td>";

		echo "<td><a class='edit'> Edit </a></td>";
		echo "<td><a class='del'> Delete</a></td></tr>";
							
			}
	}
	

?>
    </tbody>
  </table>
</div>

			</body>
			</html>