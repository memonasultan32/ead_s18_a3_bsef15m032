<?php
require('conn.php');
session_start();
if(isset($_SESSION["id"])!= true)
{
	header('Location: login.php');
}
else
	$id = $_SESSION['id'];
?>
<html>
<head>
<title>Home</title>
</head>
<body>

<?php if($_SESSION["type"]==1){ 
require('Header.php');
?>
    <h1>
        <b>Welcome Admin</b>
    </h1>

<?php }
else{ ?>

<meta charset="utf-8">
    <link rel="stylesheet" href="mystyle.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <nav style=" " class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul style="font-size:17px;font-family:Consolas" class="nav navbar-nav">
                <li class="active">
                    <a href="Home.php">Home</a>
                </li>
                <li>
                    <a href="Logout.php"><b>Log Out</b></a>
                </li>
            </ul>
        </div>
    </nav>


    <center>
        <div class="container">
			<div class="panel panel-default" style="width:500px;">
				<div class="panel-heading">Roles</div>
				<div class="panel-body">
					
					
					<?php
						$arrayRoles=array();
						$sql = "select * from user_role where userid=$id";
						$res = mysqli_query($conn,$sql);
						$records = mysqli_num_rows($res);
						
						if($records>0)
						{
							while($row = mysqli_fetch_assoc($res))
							{
								$role = $row['roleid'];
								array_push($arrayRoles,$role);
								$sql = "select name from roles where roleid=$role";
								$res = mysqli_query($conn,$sql);
								$records = mysqli_num_rows($res);
								if($records>0)
								{
									while($row = mysqli_fetch_assoc($res))
									{
										$role = $row['name'];
										echo "<li>$role</li>";							
									}
								}
							}
						}
					
					?>
					
					
				
				</div>
			</div>
			<div class="panel panel-default" style="width:500px;">
				<div class="panel-heading">Permissions</div>
				<div class="panel-body">
				
	
					<?php
					
						for($i=0;$i<count($arrayRoles);$i++)
						{
							$id = $arrayRoles[$i];
							$sql = "select * from role_permission where roleid=$id";
							$res = mysqli_query($conn,$sql);
							$records = mysqli_num_rows($res);
						
						if($records>0)
						{
							while($row = mysqli_fetch_assoc($res))
							{
								$prem = $row['permissionid'];
								$sql = "select name from permissions where permissionid=$prem";
								$result = mysqli_query($conn,$sql);
								$records = mysqli_num_rows($result);
								if($records>0)
								{
									while($row = mysqli_fetch_assoc($result))
									{
										$role = $row['name'];
										echo "<li>$role</li>";							
									}
								}
							}
						}
							
						}
						
					
					?>
					
				</div>
			</div>
		</div>

    </center>


<?php } ?>

</body>
</html>
