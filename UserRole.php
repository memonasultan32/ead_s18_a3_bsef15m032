<?php require('conn.php'); 
		require('Header.php');
				session_start();
		if(isset($_SESSION["id"])!= true)
{
	header('Location: login.php');
}
else if($_SESSION['type']==0)
{
	header('Location: login.php');
}

?>
<html>
<head>
<script>
function Main()
{


$(document).ready(function()
		{
			loadRoles();
			loadUsers();
		var editMode=false;
		global_id="";
		
		$("#save").click(function(){
				var error="";
		var role = $('#role').val();
		 var user= $('#user').val();
		
		
			if(editMode==false)
				var dataToSend = {"user":user,"role":role,"act":"saveRoleUser","mode":"new"};
			else
			{var dataToSend = {"rpid":global_id,"user":user,"role":role,"act":"saveRoleUser","mode":'edit'};
		global_id="";
		editMode=false;
		}
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					if(result.Edit.length!=0){
						
						var tableRow = $("td").filter(function() {
							return $(this).text() == result.Edit.rpid;
						}).closest("tr");
						var r = tableRow.find('td').first().text();
						var td = tableRow.find('td').first();
						td.text(r);
						td = td.next();
						td.text(result.Edit.role);
						td = td.next();
						td.text(result.Edit.user);
					
						
						
					}
					else if(result.New.length!=0)
					{
						
						
						var tab = $('#tbody');
						var row = $("<tr>");
						var col = $("<td>").text(result.New.rpid);
						row.append(col);
						col = $("<td>").text(result.New.role);
						row.append(col);
						col = $("<td>").text(result.New.user);
						row.append(col);
						col = $("<td>").html("<a class='edit'> Edit </a>");
						row.append(col);
						col = $("<td>").html("<a class='del'> Delete </a>");
						row.append(col);
						tab.append(row);
					}
				
					
				}
			};
			$.ajax(settings);
			
			return false;			
		});
		

		
	$('.del').click(function ()
		{
			
		
			 col = $(this);
			  var uid = $(this).closest('tr').find('td').first().text();
			var ret = confirm("Do you want to delete the record with id " + uid + "?");
			if(ret==true)
			{
				
				var dataToSend = {"act":"delRoleUser","id":uid};
				var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
					col.closest('tr').remove();
					
				}};
				$.ajax(settings);
				return false;
					
		
			}
			
		});
		
		
		
		$('.edit').click(function ()
		{

            var uid = $(this).closest('tr').find('td').first().text();
			global_id=uid;
			var dataToSend = {"act":"getRoleUserbyID","id":uid};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
						global_id=uid;
						editMode=true;
						$('#user').val(result.user[0].user);
						$('#role').val(result.user[0].role);
					
			}};
				$.ajax(settings);
				return false;
					
		});
		
	});//end of ready
	
	
		function gid(id) {
            return document.getElementById(id);
        }

	function clearAll()
	{
	
    gid('user').value="";
    gid('role').value="";
   
      
	}

		
	
		$('#clear').click(clearAll);
		
		
function loadRoles(){
			var dataToSend = {"act":"loadRoles"};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					
					$("#role").empty();
					for(var i=0;i<result.Roles.length;i++)
					{
						var role = result.Roles[i];
					
							var opt = $("<option value="+ role.RoleID +">"+role.Name+"</option>");
						$("#role").append(opt);						
					}
				}
			};
			$.ajax(settings);
						
		}	
		
		
		
		
		function loadUsers(){
			var dataToSend = {"act":"loadUsers"};
			var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					
					$("#user").empty();
					for(var i=0;i<result.Users.length;i++)
					{
						var user = result.Users[i];
					
							var opt = $("<option value="+ user.UserID +">"+user.Name+"</option>");
						$("#user").append(opt);						
					}
				}
			};
			$.ajax(settings);
						
		}	



	
	}






</script>
</head>
<?php 
// load data if opened in edited mode
				$role = "";
				$prem = "";
				$rpid ="";
			$pname="";
		$x="readonly='readonly'";
	
		
	
	
?>
<body onload='Main()'>
<div class="cont">
        
		<center>
		<div class="contained">
            <h1>Role-Permissions</h1>
        </div>
        <div style="padding:10px;background-color:white;width:500px;height:350px;">
            <div class="form-group">
				
				<label for="usr">ID:</label>
				<input type="text" class="form-control" id="rpid" name="rpid" value='<?php echo $rpid ?>' <?php echo $x ?> required>
                <br />
				
                <label for="usr">Roles:</label>
				
               <select class="form-control" id="role" name="role" >
			
				</select>
                <br />
				 <label for="usr">Users:</label>
				<select class="form-control" id="user" name="user" >
				
				</select>
                <br />
               
				<input style="float:right;" class="btn btn-default" type="submit" name="save" value="Save" id='save'/>
				
				<button style="float:left;" class="btn btn-default"  id='clear'>Clear</button>
				
</div>
</center>
            </div>
			</div>
			<div class="container">
  <h2>Role-Prem List</h2>      
  <table class="table table-bordered" style="background-color:white;" id="pTable">
    <thead>
      <tr>
        <th>ID</th>
        <th>Role</th>
        <th>User</th>
		<th>Edit </th>
		<th> Delete </th>
      </tr>
    </thead>
    <tbody id='tbody'>
     <?php
	
	$sql = "select * from user_role";
	$res = mysqli_query($conn ,$sql);
	$records = mysqli_num_rows($res);
	if($records>0){
		while($row = mysqli_fetch_assoc($res)) {
			
				$id = $row["id"];
				$rid = $row["roleid"];
				$pid = $row["userid"];
				
				// name of role 
				$sql1 = "select name from roles where roleid=$rid";
				$res1 = mysqli_query($conn, $sql1);
				
				while($row1 = mysqli_fetch_assoc($res1)) {
					$rname = $row1['name'];
				}
				
				// name  of permission
				$sql2 = "select name from users where userid=$pid";
				$res2 = mysqli_query($conn, $sql2);
				
				while($row2 = mysqli_fetch_assoc($res2)) {
					$pname = $row2['name'];
				}
				
				
				echo "<tr> 
				<td>$id</td>
				<td>$rname</td>
				<td>$pname</td>";

		echo "<td><a class='edit'> Edit </a></td>";
		echo "<td><a class='del'> Delete</a></td></tr>";
							
			}
	}
	

?>
    </tbody>
  </table>
</div>

			</body>
			</html>